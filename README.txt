CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Usage

INTRODUCTION
------------
Mosaic is an different approach to maintaining and displaying content in a
Drupal website.  In order to start explaining what Mosaic is all about, I'll
try to explain how it works:

A pane is the surface on which all content is rendered on.  It's basically a
block that is converted to a grid.

A tile is one of the small rectangles rendered inside a panes.  At this moment
only text is rendered on tiles but in the future we'll add media, views,
entities, menus and many more.  Feel free to suggest in the issues queue.

At this early stage of development, the Toast CSS grid framework is used for
displaying the content inside a pane.  Toast is just very flexible and easy to
use and does exactly what I had in mind but it may not be the definitive
solution to our problems.

INSTALLATION
------------
* Install the module and dependencies
* Download the Toast css framework from here: http://daneden.github.io/Toast
  and extract into the folder "sites/all/libraries/toast-css".
* ...
* Profit!

USAGE
-----
Creating panes can be done on the page 'admin/structure/mosaic' by clicking on
'Create mosaic'.  Add a pane, assign a width and save it.  You can now see the
instance of the pane in the 'block' overview (admin/structure/block) and drag
the newly created block prefixed with 'Mosaic:' onto any active region on the
page.

Once the block is at place, you can add extra tiles inside the page.

More features coming soon!
