/**
 * @file
 * Custom admin behavioral javascript.
 */
(function ($) {
  Drupal.behaviors.buildMosaicPaneAdmin = {
    attach: function (context, settings) {
      $('.mosaicgrid-edit .grid__col').each(function() {
        $(this).wrapInner('<div class="edit-tile"></div>');
      });
    }
  }
})(jQuery);
