<?php
/**
 * @file
 * Mosaic Grid schema, install and uninstall hooks.
 *
 * todo: Add sensible indexes to columns (only PK added now).
 */

/**
 * Implements hook_schema().
 */
function mosaicgrid_schema() {
  $schema = array();

  $schema['mosaicgrid_pane'] = array(
    'description' => 'The base table for mosaic panes, on which the tiles are rendered.',
    'fields' => array(
      'pid' => array(
        'description' => 'The primary identifier of the mosaic grid pane',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'The (administrative) title of this pane.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'width' => array(
        'description' => 'The maximum amount of tile units this pane can be wide.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 12, // Default grid width, can be customized
      ),
    ),
    'primary key' => array('pid'),
  );

  $schema['mosaicgrid_tile'] = array(
    'description' => 'The table for mosaic grid pane blocks, tiled upon a pane',
    'fields' => array(
      'tid' => array(
        'description' => 'The primary identifier of the mosaic grid tile',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'body' => array(
        'description' => 'Textual (or html) content of a tile',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'width' => array(
        'description' => 'The "grid" width of this tile.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'pid' => array(
        'description' => 'The pane on which the tile is placed.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'weight' => array(
        'description' => 'The weight of the tile',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('tid'),
  );

  return $schema;
}
