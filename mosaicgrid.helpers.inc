<?php
/**
 * @file
 * File bundling several helper functions.
 *
 * This is a 'helper' file, where all function
 */

/**
 * Returns a list of available pane widths.
 *
 * @return array
 */
function _mosaicgrid_pane_width_options() {
  return array(
    2   => 2,
    3   => 3,
    4   => 4,
    5   => 5,
    6   => 6,
    8   => 8,
    12  => 12);
}

/**
 * Returns a list of available tile widths.
 *
 * @param integer $pane_width
 * @return array
 */
function _mosaicgrid_tile_width_options($pane_width) {
  // When determining the maximum width of a tile, the tile width should not
  // exceed the pane width.  Otherwise an 8 units wide tile can be contained
  // inside a 6 units wide pane.
  $width_options = array();
  for ($i = 0; $i < $pane_width; $i++) {
    $width_options[$i+1] = $i+1;
  }
  return $width_options;
}
