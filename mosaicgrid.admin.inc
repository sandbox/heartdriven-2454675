<?php
/**
 * @file
 * All of the administration actions for mosaic grid.
 */

/**
 * Renders a table with a list of available panes (administration).
 *
 * @return string
 * @throws \Exception
 */
function mosaicgrid_administer_panes() {
  $header = array('Title', array('data' => 'actions', 'colspan' => 1));
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'mosaicgrid_pane');
  $result = $query->execute();

  $output = '';
  if (!empty($result['mosaicgrid_pane'])) {
    // Only render table if there are results.
    $pids = array_keys($result['mosaicgrid_pane']);
    $panes = entity_load('mosaicgrid_pane', $pids);

    $rows = array();
    foreach ($panes as $pane) {
      $rows[] = array(
        $pane->title,
        l(t('edit'), sprintf('admin/structure/mosaicgrid_/%s', $pane->pid)) . ' ' .
        l(t('delete'), sprintf('admin/structure/mosaicgrid/%s/delete', $pane->pid)),
      );
    }
    $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(),
    ));
  }
  else {
    // Otherwise render kind of a placeholder for the missing table.
    $output .= 'No panes yet.';
  }
  return $output;
}

/**
 * Prefills a form and renders it using drupal_get_form.
 *
 * @param $pid
 * @return string
 */
function mosaicgrid_edit_pane($pid) {
  $edit_form = drupal_get_form('mosaicgrid_pane_form');
  $current_pane = entity_load_single('mosaicgrid_pane', $pid);
  $edit_form['title']['#value'] = $current_pane->title;
  $edit_form['pid']['#value'] = $current_pane->pid;
  $edit_form['width']['#value'] = $current_pane->width;

  return render($edit_form);
}

/**
 * Mosaic pane form.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function mosaicgrid_pane_form($form, &$form_state) {
  $form['pid'] = array(
    '#type' => 'hidden',
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Pane title'),
    '#description' => t('The administrative name of the mosaic grid pane.'),
    '#required' => TRUE,
  );
  $form['width'] = array(
    '#type' => 'select',
    '#title' => t('Width'),
    '#description' => t('The maximum grid width of this pane.'),
    '#required' => TRUE,
    '#options' => _mosaicgrid_pane_width_options(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit action of mosaic grid pane form.
 *
 * @param array $form
 * @param array $form_state
 */
function mosaicgrid_pane_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['pid'])) {
    // Updating existing pane.
    $pane = entity_load_single('mosaicgrid_pane', $values['pid']);
    $pane->title = $values['title'];
    $pane->width = $values['width'];
    entity_save('mosaicgrid_pane', $pane);
    drupal_set_message(t('Updated pane'));
  }
  else {
    // Create a new pane.
    $pane = entity_create('mosaicgrid_pane', array(
      'type' => 'mosaicgrid_pane',
      'title' => $values['title'],
      'width' => $values['width'],
    ));
    entity_save('mosaicgrid_pane', $pane);
    drupal_set_message(t('Created pane'));
  }
  drupal_goto('admin/structure/mosaicgrid');
}

/**
 * Confirmation form when deleting a pane.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function mosaicgrid_delete_confirm_form($form, &$form_state) {
  return confirm_form(array(), t('Are you sure you want to delete this form?'), 'admin/structure/mosaicgrid/' . arg(2) . '/delete/confirm');
}

/**
 * Actually deleting form.
 */
function mosaicgrid_delete_confirm_form_submit() {
  $delid = arg(3);
  entity_delete('mosaicgrid_pane', $delid);
  drupal_set_message(t('Deleted mosaic pane.'));
  drupal_goto('admin/structure/mosaicgrid');
}


/**
 * Creation form for tiles.
 *
 * @param $form
 * @param $form_state
 * @param $pid
 * @return mixed
 */
function mosaicgrid_tile_add($form, &$form_state, $pid) {
  $current_pane = entity_load_single('mosaicgrid_pane', $pid);
  drupal_set_title(t("Add a tile to '!pane_title'", array('!pane_title' => $current_pane->title)));

  $form['pid'] = array(
    '#type' => 'hidden',
  );
  $form['tid'] = array(
    '#type' => 'hidden',
  );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Tile content'),
    '#description' => 'The HTML content of this tile.',
  );
  $form['width'] = array(
    '#type' => 'select',
    '#title' => t('Width'),
    '#description' => t('The maximum grid width of this pane.'),
    '#options' => _mosaicgrid_tile_width_options($current_pane->width),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Edit form of tiles.
 *
 * @param $tid
 * @return string
 */
function mosaicgrid_tile_edit($tid) {
  $current_tile = entity_load_single('mosaicgrid_tile', $tid);
  $edit_form = drupal_get_form('mosaicgrid_tile_add', $current_tile->pid);
  $edit_form['body']['#value'] = $current_tile->body;
  $edit_form['tid']['#value'] = $current_tile->tid;
  $edit_form['width']['#value'] = $current_tile->width;
  return render($edit_form);
}

/**
 * Submission of a new tile.
 *
 * @param $form
 * @param $form_state
 */
function mosaicgrid_tile_add_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['tid'])) {
    // Updating existing tile.
    $tile = entity_load_single('mosaicgrid_tile', $values['tid']);

    $tile->body = $values['body'];
    $tile->width = $values['width'];

    entity_save('mosaicgrid_tile', $tile);
    drupal_set_message(t('Updated tile'));
  }
  else {
    // Create a new pane.
    $tile = entity_create('mosaicgrid_tile', array(
      'type'  => 'mosaicgrid_tile',
      'body'  => $values['body'],
      'width' => $values['width'],
      'pid'   => arg(3),
    ));

    // Calculate weight of new tile.  A new tile should appear at the bottom,
    // so the formula is: weight = heaviest tile + 1
    $tile_query = new EntityFieldQuery();
    $heaviest_tile = $tile_query->entityCondition('entity_type', 'mosaicgrid_tile')
      ->propertyCondition('pid', arg(3), '=')
      ->propertyOrderBy('weight', 'DESC')
      ->range(0, 1)
      ->execute();
    $heaviest_tile = array_pop($heaviest_tile['mosaicgrid_tile']);
    if (!empty($heaviest_tile)) {
      $heaviest_tile_entity = entity_load_single('mosaicgrid_tile', $heaviest_tile->tid);
      $tile->weight = $heaviest_tile_entity->weight+1;
    }

    entity_save('mosaicgrid_tile', $tile);
    drupal_set_message(t('Created tile'));
  }
}

